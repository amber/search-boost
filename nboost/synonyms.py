#import spacy
import numpy as np
import pandas as pd
from py_openthesaurus import OpenThesaurusDb
from sentence_transformers import SentenceTransformer
import scipy.spatial
import requests
        
# =============================================================================
# OpenThesaurus
# =============================================================================

class improve_words(): 
    def __init__(self, english_only=False):
        #Load word stems from txt file
        """
        with open('synonyms/german-pos-dict-1.2/dictionary.dump', 'r') as file1: 
            print('Reading and transforming Dictionray')
            lines = file1.readlines() 
        self.stems = [line.split('\t') for line in lines]
        """
        try: 
           self.df = pd.read_csv('nboost/synonyms/german-pos-dict-1.2/dictionary.dump', sep='\t', header=None, dtype=str)
        except FileNotFoundError: 
            self.df = pd.read_csv('synonyms/german-pos-dict-1.2/dictionary.dump', sep='\t', header=None, dtype=str)
        
        #Load Thesaurus
        try:
            self.open_thesaurus = OpenThesaurusDb(host="localhost", user="philipp", passwd="test", db_name="thesaurus")
        
        except: 
            link = "https://pypi.org/project/py-openthesaurus/"
            print("Make sure your environment is set up as described here {}".format(link))

        #Init Bert for Setence Similiarity       
        if english_only: 
            self.embedder = SentenceTransformer('bert-base-nli-mean-tokens') #English Only 
        else: #Multilingual
            self.embedder = SentenceTransformer('distiluse-base-multilingual-cased') #multilingual
            #To-Do: GERMAN ONLY
            
    def find_word_stem(self, word): 
        """
        Find stem of word (singular of plural etc). 
        """
    
        stems = self.df[self.df.iloc[:,0] == word]
        unique_stem = stems.drop_duplicates(1)
        if len(unique_stem ) > 1: 
            print('Multiple word stems found') 
            #To-Do: Find right word stem with BERT or other NLP
        elif len(unique_stem) == 1: 
            stem = unique_stem[1].values.astype(str)[0]
            return stem
        else: 
            print('No word stem found')
    
    def get_thesaurus(self, word, long=True): 
        """
        Get synonyms according to Thesaurus
        """
        if long: 
            synonyms = self.open_thesaurus.get_synonyms(word=word, form="long")
            # to get the long version of synonyms as a list
        else: 
            # to get the short version of synonyms as a list
            synonyms = self.open_thesaurus.get_synonyms(word=word)
            
        synonyms = [i for i in synonyms if i != word]
        uniques = list(set(synonyms)) #Drop Duplicates 
        return uniques
        
    
    def rate_synonyms(self, list_word, query_word, other_sentence, closest_n=5):
        """
        Rate Synonyms thorugh BERT 
        Source https://github.com/UKPLab/sentence-transformers/blob/master/examples/applications/semantic_search.py
        """
        if other_sentence: 
            #Combine Word and sentence structure again
            list_word = [other_sentence.format(word) for word in list_word]
            query_word = other_sentence.format(query_word)
        
        embeddings = self.embedder.encode(list_word)
        query_embedding = self.embedder.encode(query_word)
        
        #Find nearest neighbours of sentence / word
        for query_embedding in query_embedding:
            distances = scipy.spatial.distance.cdist([query_embedding], embeddings, "cosine")[0]

            results = zip(range(len(distances)), distances)
            results = sorted(results, key=lambda x: x[1])
        
        #Just for printing 
        for idx, distance in results[0:closest_n]:
            print(list_word[idx].strip(), "(Score: %.4f)" % (1-distance))
        
        #Transform index in strings 
        bert_synonyms = [list_word[idx] for idx,distance in results[0:closest_n]] 
        return bert_synonyms
    
    
    def combine_synonyms(self, word, other_sentence=None): 
        """
        Get and rate Synonyms: 
            For multiple words, only substitute one word 
        """
        all_synonyms = self.get_thesaurus(word)
        print(all_synonyms)
        
        #If nothing was found try again with word stem
        if len(all_synonyms) == 0: 
            word = self.find_word_stem(word)
            all_synonyms = self.get_thesaurus(word)
            if len(all_synonyms) == 0: 
                return None 
            
        else:
            nlp_synonyms = self.rate_synonyms(all_synonyms, word, other_sentence, closest_n=5)
            return nlp_synonyms
        
        
    def find_spelling_corrections(self, query_sentence, language="en-US"):
        """
        Find spelling mistakes and correct them (german)
        Details for installtion (with docker are here)
        https://github.com/silvio/docker-languagetool
        """

        
        try: 
            r = requests.post('http://localhost:8010/v2/check', 
                          data = {"language": language, "text": query_sentence})
        except ConnectionError: 
            print("""Make sure language tool server is locally runnning \n
                  check https://github.com/silvio/docker-languagetool""")
            
        response_dict = dict(r.json())
        #print(response_dict)
        if len(response_dict["matches"]) > 0: #If spelling errors were found
            for error in response_dict["matches"]: #Correct them one by one 
                newstring = error['replacements'][0]["value"]
                #print(newstring)
                index = error["offset"]
                index2 = error["offset"] + error["length"]
                query_sentence = query_sentence[:index] + newstring + query_sentence[index2:]
            
            correction = query_sentence
            print('Did you mean: \n')
            print(correction)
            return correction
    
        
if __name__ == '__main__':
    synonym_finder = improve_words()
    print(synonym_finder.find_word_stem('Häusern'))
    print(synonym_finder.find_word_stem('Tunnels'))
    #To-Do: Combine find word stem with combine_synonyms
    
    
    #Make sentence embedding clear
    print(synonym_finder.combine_synonyms('Bank'))
    print(synonym_finder.combine_synonyms('Bank', other_sentence='Auf der {} sitzen'))
    print(synonym_finder.combine_synonyms('Bank', other_sentence='Die {} wurde ausgeraubt'))
    print(synonym_finder.combine_synonyms('Birne', other_sentence='Die {} im Wohnzimmer'))
    print(synonym_finder.combine_synonyms('Birne', other_sentence='Die {} schmeckt gut'))
    
    #Test spelling checker
    print(synonym_finder.find_spelling_corrections("a simpe test", "en-US"))
    

"""
# Query sentences:
queries = ['Test', 'Aufgabe', 'Schule', 'Lycee', 'High school']
query_embeddings = embedder.encode(queries)

# Find the closest 5 sentences of the corpus for each query sentence based on cosine similarity
closest_n = 5
for query, query_embedding in zip(queries, query_embeddings):
    distances = scipy.spatial.distance.cdist([query_embedding], corpus_embeddings, "cosine")[0]

    results = zip(range(len(distances)), distances)
    results = sorted(results, key=lambda x: x[1])

    print("\n\n======================\n\n")
    print("Query:", query)
    print("\nTop 5 most similar sentences in corpus:")

    for idx, distance in results[0:closest_n]:
        print(corpus[idx].strip(), "(Score: %.4f)" % (1-distance))

# =============================================================================
# Spacy Synonyms 
# =============================================================================

german_large = spacy.load("de_core_news_md")

# =============================================================================
# similiary = german_large.vocab['hund'].vector
# word = german_large.most_similar(similiary)
# most_similar = german_large.vocab.vectors.most_similar(similiary.reshape(1,similiary.shape[0]))
# =============================================================================
nlp = german_large

def most_similar(word, topn=5):
  word = nlp.vocab[str(word)]
  queries = [
      w for w in word.vocab 
      if w.is_lower == word.is_lower and w.prob >= -15 and np.count_nonzero(w.vector)
  ]

  by_similarity = sorted(queries, key=lambda w: word.similarity(w), reverse=True)
  return [(w.lower_,w.similarity(word)) for w in by_similarity[:topn+1] if w.lower_ != word.lower_]

most_similar("Stadt", topn=10)
"""