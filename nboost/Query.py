#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 16:40:59 2020

@author: philipp
"""
import NER
import synonyms

class Query():
    """
    Class which handels the Query and the transformations including all synonyms, 
    spelling errors, NER, POS etc. 
    """
    def __init__(self, text): 
        """
        Assigns User Input to Object
        """
        self.text = text
        self.word_or_phrase(text) 
    
        self.Locations = None
        self.Nouns = None
       
    def check_language(self, alq_obj):
        """
        Checks the language of the word or phrase
        Requires an initiliazed FAIR environment 
        """
        alq_obj.check_language(self)
        
        
    def set_language(self, languade_code): 
        """
        Sets the language Code accordingly
        Checks if language is supported 
        """
        if self.phrase:
            if languade_code == 'de': 
                print('Language Detected: German')
                self.language = 'de'
            elif languade_code == 'en': 
                self.language = 'en'
                print('Language Detected: English')
            else: 
                raise Exception ('Language Code:{} not supported'.format(languade_code))
                
        else: 
            #Use German for single words by default
            self.language = 'de'
        
    def word_or_phrase(self, text): 
        """
        Checks if User Input is one word or a phrase
        """
        if text.count(' ') > 1: 
            self.phrase = True
            self.word = False
        elif text.count(' ') == 0 :
            self.word = True
            self.phrase = False
        else: 
            raise Exception('Not sure if word or phrase')
            
    def locations_and_nouns(self, alq_obj):
        """
        Uses function from NER to extract Locations and Nouns from long question and phrases
        Sets self.Locations and self.Nouns
        """
        if self.word: 
            raise Exception('Extraction of Words makes no sense in a single word')
            #DBpedia Integration for that????
        
        else: 
            Entities = alq.get_locations(self.text)
            self.Locations = Entities
            print('Entites: {}'.format(Entities))
            
            tokens = alq.get_all_important_words(self.text)    
            self.Nouns = tokens
            print('Tokens: {}'.format(tokens))
                    


    def main(self, alq, synonym_finder): 
        """
        Full computation of all necessary values
        
        """
        self.check_language(alq)
        language_dict = {'de': 'de-DE', 'en' : 'en-US'}
        spelling_correction = synonym_finder.find_spelling_corrections(self.text,
                                                                       language=language_dict[self.language])
        if spelling_correction: 
            print(spelling_correction)
            #self. = ??????? 
            #To-Do: How to save spelling corrections and when are we sure to use them
            
        if self.word: #For single words:
            #self.word_stem = synonym_finder.find_word_stem(self.text) #Problem for Zeche
            #print('Word stem {}'.format(self.word_stem))
            synonyms = synonym_finder.get_thesaurus(self.text)
            rated_synonyms = synonym_finder.rate_synonyms(synonyms, self.text, other_sentence='Kohle Ruhrgebiet {}')
            #rated_synonyms = synonym_finder.rate_synonyms(synonyms, self.text, other_sentence=None)
            print(synonyms)
            print(rated_synonyms)
            
            
        elif self.phrase: #For multi word queries
            self.locations_and_nouns(alq)
            for word in self.Nouns: 
                None
                
        print('Finished for {} \n\n'.format(self.text))
        
            
if __name__ == '__main__': 
    #Tests
    #Init Pytorch Language and NER Models
    alq = NER.analyse_long_queries()
    synonym_finder = synonyms.improve_words()
    
    
    test_query = Query('Zeche')
    test_phrase = Query('Was is mein Auto')
    
    test_phrase.main(alq, synonym_finder)
    test_query.main(alq, synonym_finder)

    
    """
    test_query.check_language(alq)
    test_phrase.check_language(alq)
    
    try: 
        test_query.locations_and_nouns(alq)
    except: 
        None
    test_phrase.locations_and_nouns(alq)
    """ 
    