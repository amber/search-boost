import wikipedia 
import random 

def get_wiki_dict(search_tag): 
    """
    Get List of articles and one sentence description for search tag
    """
    wikipedia.set_lang("en")
    search_res = wikipedia.search(search_tag)
    ll_res = []
    for res in search_res:
        try:
            ll_res.append(wikipedia.summary(res, sentences=1))
        except wikipedia.exceptions.DisambiguationError:
            None
        
    #Create Elasticsearch like results
    response_dict = {
            'status': 200, 
            'headers': {
                    'content-type': "application/json; charset=UTF-8"
                        },
            'body' : {
                    "took": 3, "timed_out": False, "_shards": 
                        {"total": 5, "successful": 5, "skipped": 0, "failed": 0}, 
                    "hits": {"total": 
                        {"value": 120, "relation": "eq"},
                    "max_score": 0.99, 
                    'hits': []
                            }
                        }
            }
        
    for index, res in enumerate(ll_res): 
        response_dict['body']['hits']['hits'].append( {
                "_index" : "travel", 
                "_type" :"_doc", 
                "_id": str(index),
                "_score": random.random(),
                '_source': {
                        'passage': res
                        }
                })
        
    return response_dict
        


def change_dict(search_tag, response_dict, language="de"): 
    """
    Get List of articles and one sentence description for search tag
    """
    wikipedia.set_lang(language)
    search_res = wikipedia.search(search_tag)
    ll_res = []
    for res in search_res:
        try:
            ll_res.append(wikipedia.summary(res, sentences=2))
        except wikipedia.exceptions.DisambiguationError:
            None
        
    for index, res in enumerate(ll_res): 
        response_dict['body']['hits']['hits'].append( {
                "_index" : "travel", 
                "_type" :"wiki", 
                "_id": str(index),
                "_score": (10 - index),
                '_source': {
                        'passage': res
                        }
                })
        
    return response_dict
        
if __name__ == '__main__': 
    a = get_wiki_dict("vegas")
    
    
    """
    search_tag = "vegas"
    wikipedia.set_lang("en")
    search_res = wikipedia.search(search_tag)
    ll_res = []
    for res in search_res:
        ll_res.append(wikipedia.summary(res, sentences=1))
        
        
        
    response_dict = {
            'body' : {
                    'hits': {
                            'hits': {
                                    }
                            }
                        }
            }
        
    for index, res in enumerate(ll_res): 
        response_dict['body']['hits']['hits'][index] = {
                '_source': {
                        'passage': res
                        }
                }
    """
        
    
        
        
    