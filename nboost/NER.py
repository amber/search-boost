#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 10:50:43 2020

@author: philipp
"""
import Query

from flair.data import Sentence
from flair.models import SequenceTagger
from flair.datasets import UD_GERMAN
from flair.embeddings import OneHotEmbeddings
from flair.embeddings import MuseCrosslingualEmbeddings
import string 

# make a sentence
#sentence = Sentence('I love Berlin .')


class analyse_long_queries(): 
    """
    To-Do: 
        - Check for punctations and line breaks
        
    Loads German Pytorch Models for NER and Part of Speech Tagging 
    """
    
    def __init__(self): 
        self.tagger = SequenceTagger.load('de-ner-germeval') #de-ner-germeval
        self.tagger2 = SequenceTagger.load('de-pos')  
        
    def check_language(self, query):
        """
        Get Language Code (Only 4 Languages Supported)
        
        """
        embeddings = MuseCrosslingualEmbeddings()
        sentence = Sentence(query.text)
        language = sentence.get_language_code()
        query.set_language(language)
        return None
        
        
    def get_locations(self, text, get_all=True):
        """
        query:as simple String
        Return list of Named Entities (Organizations and Locations)
        If get_all=False then not include Organization names
        """
        sentence = Sentence(text)
        self.tagger.predict(sentence)
        
        Entities = []
        #print(sentence.to_plain_string())
        for entity in sentence.get_spans('ner'):
            if get_all: 
                Entities.append(entity.text)
            if not get_all and 'LOC' in entity.tag:
                Entities.append(entity.text)
                print(entity)
        
        return Entities
    
    def get_all_important_words(self, text): 
        """
        Collects all nouns and such things from the query
        
        See https://universaldependencies.org/tagset-conversion/de-stts-uposf.html
        """
        GOOD_WORDCLASSES = ['ADJA', 'CARD', 'NE', 'NN', 'TRUNC', 'XY', 'FM', '$(']
        important_words = []
        sentence = Sentence(text)
        self.tagger2.predict(sentence)
        print('List of dropped words')
        for token in sentence.get_spans('pos'):
            if token.tag in GOOD_WORDCLASSES:
                #print(token)
                important_words.append(token.text)
            
            #Dropped words
            else: 
                print(token.text, token.tag)
                #None
        
        #print(important_words)
        return important_words
        


if __name__ == '__main__': 
    sentence = """Die RAG Aktiengesellschaft (ehemals Ruhrkohle AG) ist ein deutsches Unternehmen mit Sitz in Essen, 
    das traditionell im Bereich des Steinkohlenbergbaus tätig ist, in den vergangenen Jahren durch Zukäufe 
    auch eine starke Chemiesparte dazugewonnen hat und darüber hinaus im Kraftwerksbau und im Immobilienbereich 
    tätig war. Am 14. September 2006 wurde der so genannte „weiße Bereich“, d. h. die Nicht-Steinkohlesparten 
    Chemie, Energie und Immobilien, in die RAG Beteiligungs AG ausgegliedert, die am 12. September 2007 in 
    Evonik Industries AG umbenannt wurde. Die RAG besteht nach Abschluss der Umorganisation als Holding der 
    Unternehmensteile RAG Anthrazit Ibbenbüren, RAG Montan Immobilien, RAG Mining Solutions und RAG Verkauf fort.
    Die RAG Aktiengesellschaft und Evonik Industries gingen Ende 2007 in den Besitz der RAG-Stiftung über. 
    2017 zog das Unternehmen vollständig von Herne nach Essen, auf das Gelände der ehemaligen Zeche Zollverein.
    Der Neubau auf dem Gelände der Zeche Zollverein kostete den Konzern 25 Millionen Euro."""
    
    sentence2 = """Bedingt durch die Kohlekrise wurde 1964 ein Förderverbund mit der Zeche Theodor hergestellt. Die Förderung verblieb auf Heinrich 1/2/3.
    Trotzdem beschloss die Heinrich Bergbau-AG die komplette Aufgabe der Bergbaubetriebe 1968. Am 1. April 1968 wurde die Stilllegung für die Förderschachtanlagen Heinrich und die Theodorschächte vollzogen. Die Schächte wurden verfüllt, ausgenommen Schacht Heinrich 3 und Holthuser Tal, die zur Wasserhaltung offenblieben. In dieser Funktion sind diese auch heute noch in Betrieb. Das Fördergerüst über Schacht Heinrich 3 ist erhalten geblieben, weithin sichtbar und ein Teil der Route der Industriekultur.
    Auf dem übrigen Zechengelände ist mittlerweile eine Wohnbebauung entstanden. Das Grubenwasser, das heute noch aus dem Schacht Heinrich 3 gehoben wird und aus stillgelegten Grubenfeldern im gesamten Essener Süden sowie Bochumer Südwesten stammt, dient als Trinkwassernotreserve der Stadt Essen. Darüber hinaus muss die Zeche Heinrich stets ausgepumpt werden, weil sonst die Oberhausener City überflutet würde. Der Grund hierfür ist, dass Überruhr-Holthausen (Schachtbezogen) drei Meter über Oberhausen liegt."""
    
    #Drop punctation in sentence
    sentence = sentence.translate(str.maketrans('', '', string.punctuation))
    
    
    
    #test_query = Query.Query(sentence2)
    test_query = Query.Query('Ich fahre nach Wanne-Eickel')
    alq = analyse_long_queries() #Load Models
    
    
    alq.check_language(test_query)
    #if test_query.phrase: 
    if True:
    # =============================================================================
    #     First check for locations 
    # =============================================================================
            
        Entities = alq.get_locations(test_query)
        print('Entites: {}'.format(Entities))
    # =============================================================================
    #     Check for Nouns etc.         
    # =============================================================================
    
        tokens = alq.get_all_important_words(test_query)    
        print('Tokens: {}'.format(tokens))
